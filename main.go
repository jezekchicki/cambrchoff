package main

import (
	"flag"
	"fmt"
	"log"
	"math"
	"os"
	"os/signal"
	"sort"
	"syscall"
	"time"

	"github.com/blackjack/webcam"
)

const maxBrightness = 255.0

func main() {
	var (
		device     string
		interval   int
		threshold  float64
		format     string
		numFrames  int
		retryCount int
		verbose    bool
	)

	// Flags
	flag.StringVar(&device, "device", "/dev/video0", "video capture device path")
	flag.IntVar(&interval, "interval", 5, "interval between brightness checks in seconds")
	flag.Float64Var(&threshold, "threshold", 10.0, "brightness change threshold percentage")
	flag.StringVar(&format, "format", "YUYV 4:2:2", "video format")
	flag.IntVar(&numFrames, "numFrames", 1, "number of frames to capture for averaging brightness")
	flag.IntVar(&retryCount, "retry", 1, "number of retries if brightness check fails")
	flag.BoolVar(&verbose, "v", false, "make output more verbose")
	flag.Parse()

	// Initialize camera
	cam, err := initializeCamera(device, format)
	if err != nil {
		log.Fatalf("Failed to initialize camera: %v", err)
	}
	defer cam.Close()

	// Signal handling
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	// Convert interval to time.Duration
	intervalDuration := time.Duration(interval) * time.Second

	// Get initial brightness
	initialBrightness, err := getAverageBrightness(cam, numFrames, verbose)
	if err != nil {
		log.Fatalf("Failed to get initial brightness: %v", err)
	}
	log.Printf("Initial brightness: %8.5f\n", initialBrightness)
	lastBrightness := initialBrightness

	counter := 0

	for {
		select {
		case sig := <-sigs:
			log.Printf("Received %s, exiting...\n", sig)
			os.Exit(0)
		case <-time.After(intervalDuration):
			currentBrightness, err := getAverageBrightness(cam, numFrames, verbose)
			if err != nil {
				log.Printf("Attempt %d/%d failed: %v", counter+1, retryCount+1, err)
				counter++
				if counter > retryCount {
					log.Fatalf("Failed to get current brightness after %d retries: %v", retryCount, err)
				}
				continue
			}

			counter = 0 // Reset counter on success

			change := (currentBrightness - lastBrightness) / maxBrightness * 100
			if verbose {
				log.Printf("Current brightness: %8.5f, Brightness change: %4.2f%%\n", currentBrightness, change)
			}

			if math.Abs(change) > threshold {
				if !verbose {
					log.Printf("Last brightness: %8.5f, Current brightness: %8.5f, Brightness change: %4.2f%%\n", lastBrightness, currentBrightness, change)
				}
				log.Printf("Brightness change exceeds threshold of %2.0f, exiting...", threshold)
				os.Exit(255)
			}
			lastBrightness = currentBrightness
		}
	}
}

func initializeCamera(device, desiredFormat string) (*webcam.Webcam, error) {
	cam, err := webcam.Open(device)
	if err != nil {
		return nil, fmt.Errorf("failed to open video device: %v", err)
	}

	// Get supported formats
	formatDesc := cam.GetSupportedFormats()
	var format webcam.PixelFormat
	found := false
	for f, d := range formatDesc {
		if d == desiredFormat {
			format = f
			found = true
			break
		}
	}

	if !found {
		formats := ""
		for _, d := range formatDesc {
			formats += fmt.Sprintln(d)
		}
		return nil, fmt.Errorf("desired format not found. Supported formats are:\n%s", formats)
	}

	// Get all supported frame sizes for the selected format
	frameSizes := cam.GetSupportedFrameSizes(format)
	if len(frameSizes) == 0 {
		return nil, fmt.Errorf("no frame sizes supported for format %v", format)
	}

	// Find the smallest resolution
	sort.Slice(frameSizes, func(i, j int) bool {
		return frameSizes[i].MaxWidth*frameSizes[i].MaxHeight < frameSizes[j].MaxWidth*frameSizes[j].MaxHeight
	})
	minWidth := frameSizes[0].MaxWidth
	minHeight := frameSizes[0].MaxHeight

	_, _, _, err = cam.SetImageFormat(format, minWidth, minHeight)
	if err != nil {
		return nil, fmt.Errorf("failed to set image format: %v", err)
	}

	log.Printf("Selected format: %v, resolution: %dx%d", formatDesc[format], minWidth, minHeight)

	return cam, nil
}

func getAverageBrightness(cam *webcam.Webcam, numFrames int, verbose bool) (float64, error) {
	// Start streaming
	if err := cam.StartStreaming(); err != nil {
		return 0, fmt.Errorf("Failed to start streaming: %v", err)
	}
	defer cam.StopStreaming()

	totalBrightness := 0.0
	for i := 0; i < numFrames; i++ {
		// Wait for a frame to be available
		if err := cam.WaitForFrame(5); err != nil {
			return 0, fmt.Errorf("failed to wait for frame: %v", err)
		}

		// Read a frame
		frame, err := cam.ReadFrame()
		if err != nil {
			return 0, fmt.Errorf("failed to read frame: %v", err)
		}

		// Calculate average brightness for the frame
		frameBrightness := 0.0
		for j := 0; j < len(frame); j += 2 {
			frameBrightness += float64(frame[j])
		}
		frameBrightness /= float64(len(frame) / 2)
		totalBrightness += frameBrightness
	}

	averageBrightness := totalBrightness / float64(numFrames)
	return averageBrightness, nil
}
